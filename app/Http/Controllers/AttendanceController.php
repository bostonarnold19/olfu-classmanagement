<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Student;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    protected $student;
    protected $attendance;

    public function __construct(Student $student, Attendance $attendance)
    {
        $this->middleware('auth');
        $this->student = $student;
        $this->attendance = $attendance;
    }

    public function studentsAttendance(Request $request)
    {
        $data = $request->all();
        $students = $this->student->where('section', $data['section'])->get();
        $section = $data['section'];
        $date = empty($data['date']) ? date('Y-m-d') : $data['date'];
        foreach ($students as $key => $value) {
            $date_absent = $students[$key]->attendances()->where('date_absent', $date)->first();
            $students[$key]['is_absent'] = empty($date_absent) ? false : true;
        }
        return view('attendance.students', compact('section', 'students', 'date'));
    }

    public function attendanceCheck(Request $request)
    {
        $data = $request->all();
        $student = $this->student->where('student_id', $data['student_id'])->first();
        $absent = $student->attendances()->where('date_absent', $data['date_absent'])->first();
        if (empty($absent)) {
            $this->attendance->fill($request->all())->save();
        } else {
            $absent->delete();
        }
        return redirect()->back();
    }

    public function attendanceReport(Request $request)
    {
        $data = $request->all();
        $section = $data['section'];
        $subject = 'MIL';
        $date = array(
            'from' => $data['from'],
            'to' => empty($data['to']) ? $data['from'] : $data['to'],
        );
        $students = [];
        $attendances = $this->attendance->whereBetween('date_absent', array($date['from'], $date['to']))->get();
        foreach ($attendances as $attendance) {
            $student = $attendance->student()->where('section', $section)->first();
            if (empty($student)) {
                continue;
            }
            $student = $student->toArray();
            // if (!isset($students[$student['student_id']])) {
            //     $student['counter'] = 0;
            //     $students[$student['student_id']] = $student;
            // }
            // if (isset($students[$student['student_id']])) {
            //     $students[$student['student_id']]['counter'] += 1;
            // }
            if (empty($students[$student['student_id']])) {
                $student['date_absent'][] = $attendance->date_absent;
                $students[$student['student_id']] = $student;
            } else {
                array_push($students[$student['student_id']]['date_absent'], $attendance->date_absent);
            }
        }
        return view('attendance.reports', compact('students', 'section', 'subject', 'date'));
    }
}
