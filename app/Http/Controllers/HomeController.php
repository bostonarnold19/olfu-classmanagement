<?php

namespace App\Http\Controllers;

use App\Student;

class HomeController extends Controller
{
    protected $student;

    public function __construct(Student $student)
    {
        $this->middleware('auth');
        $this->student = $student;
    }

    public function index()
    {
        $students = $this->student->all();
        $sections = array_unique($students->pluck('section')->toArray());
        return view('home', compact('sections'));
    }
}
