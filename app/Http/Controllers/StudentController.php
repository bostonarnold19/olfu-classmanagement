<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    protected $student;
    protected $attendance;

    public function __construct(Student $student)
    {
        $this->middleware('auth');
        $this->student = $student;
    }

    public function grades(Request $request)
    {
        $data = $request->all();
        $student = $this->student->where('student_id', $data['student_id'])->get();
        return view('student.grades', compact('student'));
    }

    public function book(Request $request)
    {
        $data = $request->all();
        $student = $this->student->where('student_id', $data['student_id'])->first();
        $student->has_book = $student->has_book ? false : true;
        $student->update();
        return redirect()->back();
    }
}
