<?php

namespace App\Http\Controllers;

use App\Student;

class SectionController extends Controller
{
    protected $student;

    public function __construct(Student $student)
    {
        $this->middleware('auth');
        $this->student = $student;
    }

    public function sectionStudents($section)
    {
        $students = $this->student->where('section', $section)->get();
        return view('section.students', compact('section', 'students'));
    }
}
