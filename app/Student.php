<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function attendances()
    {
        return $this->hasMany('App\Attendance', 'student_id', 'student_id');
    }
}
