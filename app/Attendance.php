<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'student_id', 'date_absent',
    ];

    public function student()
    {
        return $this->hasMany('App\Student', 'student_id', 'student_id');
    }
}
