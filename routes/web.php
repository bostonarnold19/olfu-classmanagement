<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect()->route('login');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/section/{section}', 'SectionController@sectionStudents')->name('get.section.students');
Route::get('/attendance', 'AttendanceController@studentsAttendance')->name('get.students.attendance');
Route::post('/attendance', 'AttendanceController@attendanceCheck')->name('post.student.attendance');
Route::get('/attendance/report', 'AttendanceController@attendanceReport')->name('get.students.attendance.report');
Route::get('/student/grades', 'StudentController@grades')->name('get.student.grades');
Route::post('/student/book', 'StudentController@book')->name('post.student.book');
