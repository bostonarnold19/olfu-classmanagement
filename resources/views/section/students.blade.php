@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Section: <b>{{ $section }}</b></div>
                <div class="panel-body">
                    <h2>Students</h2>
                    <p>Number of student: {{ $students->count() }}</p>
                    <div class="table-responsive">
                        <table class="table" id="table">
                            <thead>
                                <tr>
                                    <th>Student ID</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $student)
                                <tr>
                                    <td>{{ $student->student_id }}</td>
                                    <td>{{ $student->last_name }}</td>
                                    <td>{{ $student->first_name }}</td>
                                    <td>{{ $student->middle_name }}</td>
                                     <td>
                                        <a href="#" class="btn btn-default">Grades</a>
                                        <a href="#" onclick="event.preventDefault();document.getElementById('book-form-{{$student->id}}').submit();" class="btn {{ $student->has_book ? 'btn-success' : 'btn-default' }}">Book</a>
                                        <form id="book-form-{{$student->id}}" action="{{ route('post.student.book') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="student_id" value="{{ $student->student_id }}">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $('#table').DataTable({
        "pageLength": 100,
        "order": [[ 1, "asc" ]]
    });
});
</script>
@endsection
