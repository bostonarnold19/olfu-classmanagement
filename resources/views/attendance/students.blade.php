@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Section: <b>{{ $section }}</b></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="well">
                                <h4>Attendance Referral Form</h4>
                                <form action="{{ route('get.students.attendance.report') }}" method="get" target="_blank">
                                    <input type="hidden" name="section" value="{{ $section }}">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label" for="from">From:</label>
                                                <input type="date" name="from" class="form-control" value="{{ date('Y-m-d') }}">
                                            </div>
                                              <div class="col-md-6">
                                                <label class="control-label" for="to">To:</label>
                                                <input type="date" name="to" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <br>
                                                <button class="form-control btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="well">
                                <form action="{{ route('get.students.attendance') }}" method="get">
                                    <input type="hidden" name="section" value="{{ $section }}">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label" for="date">Date:</label>
                                                <input type="date" name="date" class="form-control" value="{{ $date }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <br>
                                                <button class="form-control btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <h2>Attendance</h2>
                    <div class="table-responsive">
                        <table class="table" id="table">
                            <thead>
                                <tr>
                                    <th>Student ID</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $student)
                                <tr>
                                    <td>{{ $student->student_id }}</td>
                                    <td>{{ $student->last_name }}</td>
                                    <td>{{ $student->first_name }}</td>
                                    <td>{{ $student->middle_name }}</td>
                                    <td>
                                        <a href="#" onclick="event.preventDefault();document.getElementById('absent-form-{{$student->id}}').submit();" class="btn {{ $student->is_absent ? 'btn-danger' : 'btn-default' }}">Absent</a>
                                        <form id="absent-form-{{$student->id}}" action="{{ route('post.student.attendance') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="date_absent" value="{{ $date }}">
                                            <input type="hidden" name="student_id" value="{{ $student->student_id }}">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $('#table').DataTable({
        "pageLength": 100,
        "order": [[ 1, "asc" ]]
    });
});
</script>
@endsection
