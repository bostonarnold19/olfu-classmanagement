<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
{{--     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style type="text/css">

          .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid black;
}
        </style>

  </head>
  <body>
    <div class="container">
      <span class="pull-right">CGS-007-12-01</span><br>
      <span class="pull-right">Our Lady of Fatima University</span><br>
      <span class="pull-right">Center for Guidance Services</span><br>
      <br>
      <h4><b>ATTENDANCE REFERRAL FORM FOR MONITORING</b></h4>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Grade/Strand/Program Year & Sec: <i>{{ $section }}<i></th>
            <th>Inclusive Dates: <i>{{ date("M jS, Y", strtotime($date['from'])) }} - {{ date("M jS, Y", strtotime($date['to'])) }}</i></th>
            <th>Name of Teacher: <i>{{ auth()->user()->first_name }} {{ str_limit(auth()->user()->middle_name, 1, '.') }} {{ auth()->user()->last_name }}</i></th>
          </tr>
          <tr>
            <th>Subject/Course: <i>{{ $subject }}</i></th>
            <th>Date Submitted: <i>{{  date("M jS, Y") }}</i></th>
            <th>Contact # of Teacher: <i>{{ auth()->user()->contact }}</i></th>
          </tr>
        </thead>
      </table>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th></th>
            <th>NAME OF STUDENT</th>
            <th>No. of Absences</th>
            <th>Dates of Absences</th>
            <th>Contact No. of Parent/Guardian</th>
            <th>Address</th>
            <th>REMARKS</th>
          </tr>
        </thead>
        <tbody>
          <?php $count = 1;?>
          @foreach($students as $student)
          <tr>
            <td>{{ $count++ }}</td>
            <td>{{ $student['last_name'] }}, {{ $student['first_name'] }} {{ str_limit($student['middle_name'], 1, '.') }}</td>
            <td>{{ count($student['date_absent']) }}</td>
            <td>
              @foreach($student['date_absent'] as $date_absent)
              {{ $date_absent }}<br>
              @endforeach
            </td>
            <td>{{ $student['address'] }}</td>
            <td>{{ $student['guardian_contact'] }}</td>
            <td></td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Professor's Signature: <br><br></th>
            <th>Counselor-Adviser's Signature: <br><br></th>
          </tr>
        </thead>
      </table>
    </div>
  </body>
</html>
